/**
 * Copyright (C), 2007-2020, 重庆英卡电子有限公司
 * FileName: Logs
 * Author:   cqnews
 * Date:     2020-11-26 14:30
 * Description: 日志
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.link510.demo.services;

import com.link510.demo.controller.CallBackController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 〈一句话功能简述〉<br>
 * 〈日志〉
 *
 * @author cqnews
 * @create 2020-11-26
 * @since 1.0.0
 */
@Service
public class Logs {

    private Logger logs = LoggerFactory.getLogger(CallBackController.class);

    /**
     * 日志写入
     *
     * @param msg 消息
     * @return
     */
    public boolean write(String msg) {

        try {
            logs.info(msg);
            return true;
        } catch (Exception ignored) {

        }
        return false;
    }

    /**
     * 日志写入
     *
     * @param msg 消息
     * @return
     */
    public boolean write(Exception ex, String msg) {

        try {
            logs.warn(msg + ":" + ex.getMessage());
            return true;
        } catch (Exception ignored) {

        }
        return false;
    }
}
