/**
 * Copyright (C), 2007-2020, 重庆英卡电子有限公司
 * FileName: IniotManages
 * Author:   cqnews
 * Date:     2020-11-26 13:53
 * Description: sdktools
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.link510.demo.services;

import com.link510.demo.exception.NotSupportedException;
import com.link510.iniot.sdk.InIoTClient;
import com.link510.iniot.sdk.domain.messages.Environment11MessageOutDTO;
import com.link510.iniot.sdk.impl.AbstractIniotClientImpl;
import com.link510.iniot.sdk.management.DeviceManagement;
import com.link510.iniot.sdk.management.MessageManagement;
import com.link510.iniot.sdk.management.SubscribeManagement;
import lombok.Getter;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 * 〈一句话功能简述〉<br>
 * 〈sdktools〉
 *
 * @author cqnews
 * @create 2020-11-26
 * @since 1.0.0
 */
@Service
public class CWMTools {

    /**
     * 设备管理客户端
     */
    @Getter
    private InIoTClient ioTClient;

    /**
     * 消息管理器
     */
    @Getter
    private MessageManagement messageManagement;

    /**
     * 设备管理器
     */
    @Getter
    private DeviceManagement deviceManagement;


    /**
     * 消息管理器
     */
    @Getter
    private SubscribeManagement subscribeManagement;

    /*
     * ApiKey:733f6f4d6d41415a87a64c26d6cda5bd
     * ApiSecret:bac9ff047e0870e872e97f38161f39ab
     */

    /**
     * 初始化设备
     */
    @PostConstruct
    public void init() {

        ioTClient = new AbstractIniotClientImpl() {

            private static final long serialVersionUID = 5527196152642058766L;

            @Override
            public String getApiKey() {
                return "733f6f4d6d41415a87a64c26d6cda5bd";
            }

            @Override
            public String getApiSecret() {
                return "bac9ff047e0870e872e97f38161f39ab";
            }
        };


        messageManagement = new MessageManagement(ioTClient);
        deviceManagement = new DeviceManagement(ioTClient);
        subscribeManagement = new SubscribeManagement(ioTClient);
    }


}
