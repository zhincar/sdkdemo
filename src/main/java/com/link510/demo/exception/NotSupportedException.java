/**
 * Copyright (C), 2007-2020, 重庆英卡电子有限公司
 * FileName: NotSupportedException
 * Author:   cqnews
 * Date:     2020-11-26 14:01
 * Description: NotSupportedException
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.link510.demo.exception;

/**
 * 〈一句话功能简述〉<br> 
 * 〈NotSupportedException〉
 *
 * @author cqnews
 * @create 2020-11-26
 * @since 1.0.0
 */
public class NotSupportedException extends Exception {

    private static final long serialVersionUID = 4686141843302811225L;

    public NotSupportedException() {
    }

    public NotSupportedException(String message) {
        super(message);
    }

    public NotSupportedException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotSupportedException(Throwable cause) {
        super(cause);
    }

    public NotSupportedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
