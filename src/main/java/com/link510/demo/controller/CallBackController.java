package com.link510.demo.controller;

import com.link510.demo.services.Logs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author cqnews
 * @version 1.0
 * @date 2020-11-26 13:36
 */
@RestController(value = "NewNBIoTCTCCCallBackController")
@RequestMapping("callbak")
public class CallBackController {


    @Autowired
    private Logs logs;

    /**
     * 接收电信NB设备消息
     *
     * @param param 参数
     * @return ResponseEntity
     */
    @RequestMapping("test")
    public ResponseEntity<HttpStatus> test(@RequestBody(required = false) String param) {

        logs.write(param);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
