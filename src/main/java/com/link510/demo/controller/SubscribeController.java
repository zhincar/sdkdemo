/**
 * Copyright (C), 2007-2020, 重庆英卡电子有限公司
 * FileName: SubscribeController
 * Author:   cqnews
 * Date:     2020-11-26 14:35
 * Description: subscribe
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.link510.demo.controller;

import com.link510.demo.services.CWMTools;
import com.link510.demo.services.Logs;
import com.link510.iniot.sdk.domain.subscribes.SubscribeOutDTO;
import com.link510.iniot.sdk.domain.subscribes.SubscribeSendDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈subscribe〉
 *
 * @author cqnews
 * @create 2020-11-26
 * @since 1.0.0
 */
@RestController
@RequestMapping(value = "subscribe")
public class SubscribeController {


    @Autowired
    private CWMTools cwmTools;

    @Autowired
    private Logs logs;

    /**
     * 订阅列表
     *
     * @return ListMessageOutDTO
     */
    @RequestMapping(value = "list")
    public List<SubscribeOutDTO> list() {
        return cwmTools.getSubscribeManagement().list();
    }

    /**
     * 订阅列表
     *
     * @return ListMessageOutDTO
     */
    @RequestMapping(value = "send")
    public List<SubscribeOutDTO> send() {

        String title = "测试订阅";
        Integer productId = 14;
        String protocol = "0XB0";
        String httpUrl = "http://114.215.40.196:8000/callbak/test";
        Integer format = 0;

        SubscribeSendDTO subscribeSendDTO =
                SubscribeSendDTO.builder()
                        .title(title)
                        .productId(productId)
                        .protocol(protocol)
                        .httpUrl(httpUrl)
                        .format(format).build();

        boolean isSuccess = cwmTools.getSubscribeManagement().send(subscribeSendDTO);
        if (isSuccess) {
            logs.write("订阅成功");
        } else {
            logs.write("订阅失败");
        }

        return cwmTools.getSubscribeManagement().list();
    }

    /**
     * 订阅列表
     *
     * @return ListMessageOutDTO
     */
    @RequestMapping(value = "cancel")
    public List<SubscribeOutDTO> cancel() {

        Integer productId = 14;
        String protocol = "0XB0";


        boolean isSuccess = cwmTools.getSubscribeManagement().cancel(productId, protocol);
        if (isSuccess) {
            logs.write("订阅取消成功");
        } else {
            logs.write("订阅取消失败");
        }

        return cwmTools.getSubscribeManagement().list();
    }

    /**
     * 订阅测试推送
     *
     * @return ListMessageOutDTO
     */
    @RequestMapping(value = "test")
    public String test() {

        Integer productId = 14;
        String protocol = "0XB0";


        boolean isSuccess = cwmTools.getSubscribeManagement().cancel(productId, protocol);
        if (isSuccess) {
            return "订阅推送成功";
        } else {
            return "订阅推送失败";
        }

    }

}
