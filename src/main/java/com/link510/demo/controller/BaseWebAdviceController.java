/**
 * Copyright (C), 2007-2020, 重庆英卡电子有限公司
 * FileName: a2
 * Author:   cqnews
 * Date:     2020-11-26 15:17
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.link510.demo.controller;

/**
 * 〈一句话功能简述〉<br>
 * 〈〉
 *
 * @author cqnews
 * @create 2020-11-26
 * @since 1.0.0
 */

import com.link510.iniot.sdk.exception.InIotException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 全局自定义异常处理
 *
 * @author cqnews
 */
@RestControllerAdvice
public class BaseWebAdviceController {


    /**
     * 拦截捕捉自定义异常 CWMAPIException.class
     *
     * @param ex CWMAPIException
     * @return CWMAPIException
     */
    @ExceptionHandler(value = InIotException.class)
    public String apiErrorHandler(InIotException ex) {
        return "API接口异常,异常信息:" + ex.getMessage();
    }
}
