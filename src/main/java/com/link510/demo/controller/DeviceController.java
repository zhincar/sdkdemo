/**
 * Copyright (C), 2007-2020, 重庆英卡电子有限公司
 * FileName: DeviceController
 * Author:   cqnews
 * Date:     2020-11-26 14:16
 * Description: 设备管理
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.link510.demo.controller;

import com.link510.demo.services.CWMTools;
import com.link510.iniot.sdk.domain.devices.DeviceOutDTO;
import com.link510.iniot.sdk.domain.devices.ProductOutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈设备管理〉
 *
 * @author cqnews
 * @create 2020-11-26
 * @since 1.0.0
 */
@RestController
@RequestMapping(value = "device")
public class DeviceController {

    @Autowired
    private CWMTools cwmTools;

    /**
     * 设备管理列表
     *
     * @return List<DeviceOutDTO>
     */
    @RequestMapping(value = "list")
    public List<DeviceOutDTO> list() {
        return cwmTools.getDeviceManagement().list();
    }

    /**
     * 查询设备
     *
     * @return List<DeviceOutDTO>
     */
    @RequestMapping(value = "find")
    public DeviceOutDTO find(@RequestParam(defaultValue = "85854") String deviceSN) {
        return cwmTools.getDeviceManagement().find(deviceSN);
    }

    /**
     * 查询设备产品信息
     *
     * @return List<DeviceOutDTO>
     */
    @RequestMapping(value = "product")
    public ProductOutDTO product(@RequestParam(defaultValue = "85854") String deviceSN) {
        return cwmTools.getDeviceManagement().product(deviceSN);
    }
}
