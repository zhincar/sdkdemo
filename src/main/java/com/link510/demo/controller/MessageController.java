/**
 * Copyright (C), 2007-2020, 重庆英卡电子有限公司
 * FileName: Message
 * Author:   cqnews
 * Date:     2020-11-26 14:21
 * Description:
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.link510.demo.controller;

import com.link510.demo.services.CWMTools;
import com.link510.iniot.sdk.domain.messages.ListMessageOutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 〈消息控制器〉<br>
 * 〈〉
 *
 * @author cqnews
 * @create 2020-11-26
 * @since 1.0.0
 */
@RestController
@RequestMapping(value = "message")
public class MessageController {


    @Autowired
    private CWMTools cwmTools;


    /**
     * 设备列表
     *
     * @param deviceSN 设备编号
     * @return ListMessageOutDTO
     */
    @RequestMapping(value = "list")
    public ListMessageOutDTO list(@RequestParam(defaultValue = "85854") String deviceSN) {
        return cwmTools.getMessageManagement().list(deviceSN, 10, 1);
    }


    /**
     * 设备列表
     *
     * @param deviceSN 设备编号
     * @return ListMessageOutDTO
     */
    @RequestMapping(value = "last")
    public String last(@RequestParam(defaultValue = "85854") String deviceSN) {
        return cwmTools.getMessageManagement().last(deviceSN, "0XB0", String.class);
    }


    /**
     * 设备列表
     *
     * @param msgId 消息Id
     * @return ListMessageOutDTO
     */
    @RequestMapping(value = "show")
    public String show(@RequestParam(defaultValue = "6940212") Integer msgId) {
        return cwmTools.getMessageManagement().show(msgId, String.class);
    }
}
