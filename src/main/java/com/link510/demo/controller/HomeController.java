/**
 * Copyright (C), 2007-2020, 重庆英卡电子有限公司
 * FileName: HomeController
 * Author:   cqnews
 * Date:     2020-11-26 13:52
 * Description: HomeController
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
package com.link510.demo.controller;

import com.link510.demo.services.CWMTools;
import com.link510.iniot.sdk.domain.devices.DeviceOutDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 〈一句话功能简述〉<br>
 * 〈HomeController〉
 *
 * @author cqnews
 * @create 2020-11-26
 * @since 1.0.0
 */
@RestController
public class HomeController {


    /**
     * 首页
     *
     * @return String
     */
    @RequestMapping(value = {"", "/", "/index"})
    public String index() {
        System.out.println("welcom to 510link.com.");
        return "welcom to 510link.com.";
    }


}
